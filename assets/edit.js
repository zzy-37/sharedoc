const input = document.querySelector('input[type=file]')
const list = document.querySelector('#fileList')
const editor = document.querySelector('textarea')
input.addEventListener('change', updateFiles)
fetch("/assets/doc.txt").then(res => res.text()).then(data => editor.textContent = data)
function updateFiles() {
    while(list.firstChild)
        list.removeChild(list.firstChild)
    const curFiles = input.files
    for (file of curFiles) {
        const listItem = document.createElement('li')
        listItem.textContent = file.name
        if (validFileType(file)) {
            const button = document.createElement('button')
            button.type = 'button'
            button.textContent = 'insert to document'
            button.value = file.name
            button.addEventListener('click', insertImg)
            listItem.appendChild(button)
        }
        list.appendChild(listItem)
    }
}
function insertImg() {
    editor.value += `\n![${this.value}](/assets/files/${this.value})\n`
}
// https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types
const fileTypes = [
  "image/apng",
  "image/bmp",
  "image/gif",
  "image/jpeg",
  "image/pjpeg",
  "image/png",
  "image/svg+xml",
  "image/tiff",
  "image/webp",
  "image/x-icon"
];
function validFileType(file) {
  return fileTypes.includes(file.type);
}
