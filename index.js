import http from 'http'
import util from 'util'
import formidable from 'formidable'
import { access, constants, appendFileSync, mkdirSync, readFileSync, writeFileSync, copyFileSync, readdirSync, statSync } from 'fs'
import { marked } from 'marked'

const hostname = "127.0.0.1"
const port = 8000

const doc = '/assets/doc.txt'
const log_dir = '/assets/logs'
const file_dir = '/assets/files'

const http_head = { 'Content-Type': 'text/html; charset=utf-8' }
const html_head = '<!DOCTYPE html><html lang="en"><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1">'

marked.setOptions({
  gfm: true,
  breaks: true
})

function format_byte(n, i = 0) {
  const sfx = ["B", "KB", "MB", "GB", "TB"]
  if (n < 1000) return `${+n.toFixed(2)}${sfx[i]}`
  else return format_byte(n / 1024, ++i)
}

function dir_view(path) {
  let data = `<style>span { float: right } li { margin: 1em 0; clear: both }</style><h1>${path}</h1><hr><ul>`
  for (const file of readdirSync('.' + path)) {
    const s = statSync(`.${path}/${file}`)
    let sfx = ''
    if (s.isDirectory()) sfx = '/'
    data += `<li><a href="${path}/${file}">${file}${sfx}</a><span>${format_byte(s.size)}, ${s.mtime.toLocaleString()}</span></li>`
  }
  return data
}

function gen_html_head(title) { return html_head + `<title>${title}</title>` }

access('.' + doc, constants.F_OK, (err) => {
  if (err) {
    console.log(`${doc} does not exist, creating...`);
    writeFileSync('.' + doc, '')
  }
})
mkdirSync('.' + log_dir, { recursive: true })
mkdirSync('.' + file_dir, { recursive: true })

const server = http.createServer((req, res) => {
  const uri = decodeURI(req.url)
  console.log(`${new Date().toLocaleString()} ${req.method} ${uri}`)
  if (req.method == 'POST') {
    const form = formidable({
      uploadDir: '.' + file_dir,
      multiples: true,
      filename: (name, ext, part, form) => { return part.originalFilename }
    })
    form.parse(req, (err, fields, files) => {
      try {
        if (uri == '/file') {
          const names = Array.from(files.upload, file => file.newFilename)
          console.log(`uploaded: ${names}`)
          res.writeHead(303, { 'location': uri });
          res.end()
          return
        }
        if (uri == '/'){
          const name = files.image.newFilename
          console.log(`uploaded: ${name}`)
          appendFileSync('.' + doc, `\n![${name}](.${file_dir}/${name})\n`)
          res.writeHead(303, { 'location': uri });
          res.end()
          return
        }
        if (uri == '/edit') {
          writeFileSync('.' + doc, fields.text)
          copyFileSync('.' + doc, `.${log_dir}/${(new Date).valueOf()}.txt`)
          res.writeHead(303, { 'Location': '/' })
          res.end()
          return
        }
        res.writeHead(400)
        res.end('Bad Request');
        return
      } catch (err) {
        res.writeHead(err.httpCode || 400, { 'Content-Type': 'text/plain' })
        res.end(String(err));
        return
      }
    })
    return
  }
  try {
    if (uri == '/favicon.ico') {
      res.writeHead(204)
      res.end()
      return
    }
    if (uri == '/doc') {
      res.writeHead(200, http_head)
      res.write(gen_html_head('Document'))
      res.end(marked.parse(readFileSync('.' + doc, 'utf-8')))
      return
    }
    if (uri == '/edit') {
      res.writeHead(200, http_head)
      res.write(html_head)
      res.end(readFileSync('./edit.html', 'utf-8'))
      return
    }
    if (uri == '/file') {
      res.writeHead(200, http_head)
      res.write(html_head)
      res.write(readFileSync('./file.html'))
      res.end(dir_view(file_dir))
      return
    }
    if (uri == '/') {
      res.writeHead(200, http_head)
      res.write(html_head)
      res.write(readFileSync('./index.html'))
      res.end(marked.parse(readFileSync('.' + doc, 'utf-8')))
      return
    }
    if (uri.substring(0, 7) == '/assets') {
      if (statSync('.' + uri).isDirectory()) {
        res.writeHead(200, http_head)
        res.write(gen_html_head(uri))
        res.end(dir_view(uri))
      } else res.end(readFileSync('.' + uri))
      return
    }
    res.writeHead(400)
    res.end('Bad Request')
    return
  } catch (err) {
    console.log(JSON.stringify(err))
    res.writeHead(err.httpCode || 400)
    res.end(String(err))
    return
  }
})

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
})
